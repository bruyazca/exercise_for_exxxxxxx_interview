package com.eg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eg.singlylinked.SinglyLinkedListNodeSpecialOps;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Run normal test cases for 'n' in [0..10]
     *
     * This includes coverage for 'null' cases.
     *
     * For each value of 'n', this test uses the 'EgVisitor' class to visit each
     * generated circular list, gather statistics across all generated circular
     * lists, and summarizes and verifies that expected conditions are met.
     */
    public void testNormalSinglyLinkedListSpecialOperations() {

        for (int nn = 0; nn <= 10; ++nn) {

            logger.info("");
            logger.info("======== Running test with n = {} ========", nn);
            logger.info("");

            if (0 == nn) {

                logger.info("generating null, i.e., empty list");
            }
            else {
                logger.info("generating list with nodes [{}..{}]", 0, nn - 1);
            }
            logger.info("");
            final EgSinglyLinkedListNode singlyLinkedList = App.makeExampleSinglyLinkedListWithNNodes(nn);
            App.dumpExampleSinglyLinkedListPerhapsMadeCircular("test list for n = " + nn, singlyLinkedList);

            logger.info("");
            logger.info(
                    "*** Running special algorithm with a visitor to analyze all circular lists and summarize results ***");

            final EgVisitor visitor = new EgVisitor(nn);
            boolean threwException = true;
            try {
                SinglyLinkedListNodeSpecialOps.specialSinglyLinkedListOperation(singlyLinkedList, visitor);
                threwException = false;
            }
            catch (final Throwable t) {
                logger.error("Unexpected exception for test case with nn=" + nn, t);
            }
            assertFalse(threwException);

            final boolean allOk = visitor.summarizeResultAndIndicateWhetherExpectedOutcome();
            assertTrue(allOk);

            logger.info("");
        }

        logger.info("");
    }

    public void testCircularList() {

        final EgSinglyLinkedListNode node1 = new EgSinglyLinkedListNode(1);
        final EgSinglyLinkedListNode node2 = new EgSinglyLinkedListNode(2);
        final EgSinglyLinkedListNode node3 = new EgSinglyLinkedListNode(3);

        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node1);

        boolean threwException = true;
        try {

            logger.warn("we expect the special function to throw an exception on this circular list");

            // we expect this to throw! it's a circular list!
            SinglyLinkedListNodeSpecialOps.specialSinglyLinkedListOperation(node1);

            logger.error("the special function did not throw an exception!");
            threwException = false;
        }
        catch (final Throwable t) {
            logger.info("caught an exception as expected", t);
            assertTrue(t.getMessage().equals("the input list is not strictly a 'linear' singly-linked list"));
        }
        assertTrue(threwException);
        logger.info("NOTE:  the exception listed immediately above is expected");
    }

    public void testNonCircularListWithCycleAsDescribedInSpecialFunctionDocumentation() {

        // for the structure of this non-circular, but also "non-linear"
        // singly-linked list, see documentation in the implementation of the
        // special function .... well, we'll copy it here!

        // @formatter:off
        //
        //   singlyLinkedNonCircularList
        //     |
        //     |
        //     v
        //
        //     1  -->  2 --> 3 --> 4 --> 5 --> 6 --> 7
        //
        //                         ^                 |
        //                         |                 |
        //                         |                 v
        //
        //                         9 <-------------- 8
        //
        // @formatter:on

        final EgSinglyLinkedListNode node1 = new EgSinglyLinkedListNode(1);
        final EgSinglyLinkedListNode node2 = new EgSinglyLinkedListNode(2);
        final EgSinglyLinkedListNode node3 = new EgSinglyLinkedListNode(3);
        final EgSinglyLinkedListNode node4 = new EgSinglyLinkedListNode(4);
        final EgSinglyLinkedListNode node5 = new EgSinglyLinkedListNode(5);
        final EgSinglyLinkedListNode node6 = new EgSinglyLinkedListNode(6);
        final EgSinglyLinkedListNode node7 = new EgSinglyLinkedListNode(7);
        final EgSinglyLinkedListNode node8 = new EgSinglyLinkedListNode(8);
        final EgSinglyLinkedListNode node9 = new EgSinglyLinkedListNode(9);

        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);
        node4.setNext(node5);
        node5.setNext(node6);
        node6.setNext(node7);
        node7.setNext(node8);
        node8.setNext(node9);
        node9.setNext(node4); // introduces a cycle!!!

        boolean threwException = true;
        try {

            logger.warn("we expect the special function to throw an exception on this list with a cycle");

            // we expect this to throw! it's a non-strictly-linear list!
            SinglyLinkedListNodeSpecialOps.specialSinglyLinkedListOperation(node1);

            logger.error("the special function did not throw an exception!");
            threwException = false;
        }
        catch (final Throwable t) {
            logger.info("caught an exception as expected", t);
            assertTrue(t.getMessage().equals("the input list is not strictly a 'linear' singly-linked list"));
        }
        assertTrue(threwException);
        logger.info("NOTE:  the exception listed immediately above is expected");
    }
}
