package com.eg.singlylinked;

/**
 * Singly-linked list node. There appears to be no standard Java
 * singly-linked-list implementation, so we create the node here. Users can
 * subclass from this to create their own singly-linked list with additional
 * required fields.
 *
 * Note also that to represent a singly-linked list, we'll just refer to its
 * head node, rather than creating a separate class to represent the list as a
 * whole (as would be done for standard Java implementations where the object
 * that is the interface to, for example, a doubly-linked list, is separate from
 * the underlying nodes that represent the list elements).
 *
 * Nodes of this type can be used to create singly-linked circular lists as well
 * (or ... won't go into this).
 */
public class SinglyLinkedListNode {

	SinglyLinkedListNode next = null;

	/**
	 * Construct a singly-linked list node (and also represent the list itself).
	 * Client code uses multiple such objects to construct their own list using
	 * the 'setNext()' function as appropriate.
	 */
	public SinglyLinkedListNode() {
	}

	/**
	 * Gets the next node in the singly-linked list -- is 'protected' so
	 * subclasses can add type-casting as appropriate
	 *
	 * @return the next node in the singly-linked list
	 */
	protected SinglyLinkedListNode getNext() {
		return next;
	}

	public void setNext(final SinglyLinkedListNode next) {
		this.next = next;
	}
}
