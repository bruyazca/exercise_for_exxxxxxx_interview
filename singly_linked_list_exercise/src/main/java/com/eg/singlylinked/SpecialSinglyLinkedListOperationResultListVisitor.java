package com.eg.singlylinked;

/**
 * a special visitor that helps analyze/verify results of the algorithm
 */
public interface SpecialSinglyLinkedListOperationResultListVisitor<T extends SinglyLinkedListNode> {

    /**
     * Visits each list resulting from the special operation
     *
     * @param oneResultListOrNull
     *            one of the operation's result lists -- will be 'null' only for
     *            the case that the original input to the special function was
     *            'null'
     */
    public void visitOneResultList(final T oneResultListOrNull);
}
