package com.eg.singlylinked;

/**
 * a class that holds static functions that are related to type
 * 'SinglyLinkedListNode' but that are specialized enough or too esoteric to
 * belong in the 'SinglyLinkedListNode' class interface
 */
public class SinglyLinkedListNodeSpecialOps {

    /**
     * Performs the special operation on the singly-linked list without creating
     * any additional nodes or memory beyond what's in the stack frame for the
     * underlying implementing function. This *destructive* operation is
     * described in more detail in the underlying implementing function.
     *
     * @param singlyLinkedNonCircularList
     *            the singly-linked list on which to perform the special
     *            operation -- must not be circular
     * @throws Exception
     *             for circular or pseudo-circular list inputs
     */
    public static void specialSinglyLinkedListOperation(final SinglyLinkedListNode singlyLinkedNonCircularList)
            throws Exception {

        // perform the operation with no visitor, so that technically the
        // algorithm itself creates no additional memory

        specialSinglyLinkedListOperation(singlyLinkedNonCircularList, null);
    }

    /**
     * Performs a *destructive* special operation on a singly-linked list
     * without creating any additional nodes or memory beyond what's in the call
     * stack frame for this function. Note that the visitor, if specified, may
     * choose to accumulate the resulting lists produced in some other
     * memory-impacting way ... that is outside the purview of this operation's
     * implementation.
     *
     * Here's a conceptual description of the operation ... given that the input
     * singly-linked list has 'N' nodes, and that we term the nodes as '0' (for
     * the node pointed to by 'singlyLinkedNonCircularList') to 'N-1' (the last
     * node) the special operation does the following:
     * <ul>
     * <li>takes nodes '0' and 'N-1' and turns them into a singly-linked
     * circular list</li>
     * <li>takes nodes '1' and 'N-2' and turns them into a singly-linked
     * circular list</li>
     * <li>takes nodes '2' and 'N-3' and turns them into a singly-linked
     * circular list</li>
     * <li>...</li>
     * <li>takes nodes 'K' and 'N-K' and turns them into a singly-linked
     * circular list</li>
     * <li>...</li>
     * <li>until all nodes have been used</li>
     * <li>... NOTE: It is conceptually possible that the last such constructed
     * circular list involves only one, the "middle", node ...</li>
     * </ul>
     *
     * More formally, another description:
     * <li>for each 'K' such that K is between 0 and floor((N - 1) / 2.0) ...
     * </li>
     * <li>... a circular list is constructed from the two nodes or single node
     * addressed by 'K' and 'N - K' ...</li>
     * <li>... so that for even 'N' there will result N/2 two-node circular
     * lists ...</li>
     * <li>... and so that for odd 'N' there will result (N-1)/2 two-node
     * circular lists and a one one-node circular list</li>
     *
     * The operation handles a 'null' input by returning early and ignoring
     * input.
     *
     * The operation throws an exception if it detects the list is circular or
     * pseudo-circular (i.e., the first nodes may not indeed be part of a
     * circle, but the algorithm will detect if / when there is an eventual
     * cycle in the links pointed to).
     *
     * @param singlyLinkedNonCircularList
     *            the singly-linked list on which to perform the special
     *            *destructive* operation -- the list must not be circular
     * @param visitor
     *            any special visitor the client wants to provide
     * @throws Exception
     *             for circular or pseudo-circular list inputs
     */
    public static void specialSinglyLinkedListOperation(final SinglyLinkedListNode singlyLinkedNonCircularList,
            final SpecialSinglyLinkedListOperationResultListVisitor visitor) throws Exception {

        // we avoid any recursive elements in this implementation to avoid stack
        // exhaustion issues

        // -- handle special 'null' input case ... tell visitor about the
        // special case -- quit after this special-case handling

        if (null == singlyLinkedNonCircularList) {

            if (null != visitor) {
                visitor.visitOneResultList(null);
            }

            return;
        }

        // -- find the last and "pre-middle" nodes of the non-circular list

        SinglyLinkedListNode lastNode = singlyLinkedNonCircularList;
        SinglyLinkedListNode preMiddleNode = null;
        boolean advancePreMiddle = true;
        while (null != lastNode.getNext()) {

            lastNode = lastNode.getNext();

            if (advancePreMiddle) {
                preMiddleNode = null == preMiddleNode ? singlyLinkedNonCircularList : preMiddleNode.getNext();
            }
            advancePreMiddle = !advancePreMiddle;

            // detect if there's a special circular or pseudo-circular condition
            // ... checking for 'lastNode == singlyLinkedNonCircularList' is not
            // sufficient! An example of a strange input list that would cause
            // problems but not be caught by the 'lastNode ==
            // singlyLinkedNonCircularList' test would be the following:
            //
            // @formatter:off
			//
			//   singlyLinkedNonCircularList
			//     |
			//     |
			//     v
			//
			//     1  -->  2 --> 3 --> 4 --> 5 --> 6 --> 7
			//
			//                         ^                 |
			//                         |                 |
			//                         |                 v
			//
			//                         9 <-------------- 8
			//
			// @formatter:on

            if (lastNode == preMiddleNode) {
                throw new Exception("the input list is not strictly a 'linear' singly-linked list");
            }
        }

        SinglyLinkedListNode postMiddleNode = null;

        // -- compute the middle (relevant only for odd-length lists) and
        // post-middle nodes

        final boolean isOddLength = advancePreMiddle; // invariant works out
                                                      // this way

        SinglyLinkedListNode middleNode = null;
        if (isOddLength) {

            middleNode = null == preMiddleNode ? singlyLinkedNonCircularList : preMiddleNode.getNext();

            postMiddleNode = middleNode.getNext();
        }
        else {

            postMiddleNode = null == preMiddleNode ? null : preMiddleNode.getNext();
        }

        // -- special handling for the middle-node in odd-length lists

        if (null != middleNode) {

            middleNode.setNext(middleNode);

            if (null != visitor) {
                visitor.visitOneResultList(middleNode);
            }
        }

        // -- reverse the second part of the list starting with postMiddleNode
        // ... (note that it doesn't seem there's a way to combine the
        // second-half-reversal with the two-node-circular-list creation ... so
        // we do the reverse first, then the merge)

        if (null != preMiddleNode) {
            preMiddleNode.setNext(null);
        }

        // note: no null-check required
        final SinglyLinkedListNode middleHalfReversed = reverseKnownNonCircularList(postMiddleNode);

        // -- do the merge, only if the middle-half is not null

        SinglyLinkedListNode firstHalfNode2, secondHalfNode2;
        for (SinglyLinkedListNode firstHalfNode = singlyLinkedNonCircularList, secondHalfNode = middleHalfReversed; null != secondHalfNode; firstHalfNode = firstHalfNode2, secondHalfNode = secondHalfNode2) {

            // so we continue down the lists next iteration

            firstHalfNode2 = firstHalfNode.getNext();
            secondHalfNode2 = secondHalfNode.getNext();

            // make a circular list

            firstHalfNode.setNext(secondHalfNode);
            secondHalfNode.setNext(firstHalfNode);

            if (null != visitor) {
                visitor.visitOneResultList(firstHalfNode);
            }
        }
    }

    /**
     * Reverses the second half of a non-circular and non-pseudo-circular
     * singly-linked list. We assume the input is a strict linear singly-linked
     * list.
     *
     * An implementation of this might be a candidate for a different
     * "special operation" but we don't expose it as such for now.
     *
     * @param listNode
     *            the beginning of the list to be reversed
     * @return the reversed form of the list.
     */
    private static SinglyLinkedListNode reverseKnownNonCircularList(final SinglyLinkedListNode listNode) {

        // we avoid a recursive implementation due to stack exhaustion issues

        // DO_NOT: if (null == listNode) ... no special case required

        SinglyLinkedListNode curReversedHead = null;

        SinglyLinkedListNode nextNode;
        for (SinglyLinkedListNode curNode = listNode; null != curNode; curNode = nextNode) {

            nextNode = curNode.getNext();

            curNode.setNext(curReversedHead);
            curReversedHead = curNode;
        }

        return curReversedHead;
    }
}
