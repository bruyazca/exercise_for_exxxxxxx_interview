package com.eg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eg.singlylinked.SpecialSinglyLinkedListOperationResultListVisitor;

/**
 * a visitor that will examine the algorithm output lists, print some details,
 * gather some statistics, and summarize results
 */
public class EgVisitor implements SpecialSinglyLinkedListOperationResultListVisitor<EgSinglyLinkedListNode> {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final int n;

    private int numOverallVisits = 0;
    private int numNullVisits = 0;
    private int numNonNullVisits = 0;
    private int numOneNodeCircularLists = 0;
    private int numTwoNodeCircularLists = 0;
    private int numNonOneOrTwoNodeLists = 0;
    private int numNonAddToNMinusOne = 0;

    /**
     * A visitor to examine results of the special algorithm
     *
     * @param n
     *            the original 'n' value for the length of the singly-linked
     *            list
     */
    public EgVisitor(final int n) {
        this.n = n;
    }

    /**
     * visit a result list
     *
     * @param oneResultListOrNull
     *            one of the lists the special algorithm generates
     */
    public void visitOneResultList(final EgSinglyLinkedListNode oneResultListOrNull) {

        logger.debug("visit {} for case n=={}", numOverallVisits++, n);

        // verify

        EgSinglyLinkedListNode otherNode = null;
        boolean doSumTest = false;
        if (null == oneResultListOrNull) {

            logger.debug("  visit:  'null'");

            ++numNullVisits;
        }
        else {

            ++numNonNullVisits;

            otherNode = oneResultListOrNull.getNext(); // might be same node
                                                       // again!

            if (oneResultListOrNull == otherNode) {

                logger.debug("  visit:  one-node circular list value=={}", oneResultListOrNull.getValue());

                ++numOneNodeCircularLists;

                doSumTest = true;
            }
            else {

                final EgSinglyLinkedListNode otherOtherNode = null == otherNode ? null : otherNode.getNext();

                if (oneResultListOrNull == otherOtherNode) {

                    logger.debug("  visit:  two-node circular list values=={},{}", oneResultListOrNull.getValue(),
                            otherNode.getValue());

                    ++numTwoNodeCircularLists;

                    doSumTest = true;
                }
                else {

                    logger.error("  ERROR visit:  not a one- or two-node circular list!");

                    ++numNonOneOrTwoNodeLists;
                }
            }
        }

        if (null != otherNode && doSumTest) {

            final int sum = oneResultListOrNull.getValue() + otherNode.getValue();
            if (n - 1 != sum) {

                logger.error("  ERROR sum:  expected sum to be {} but is actually {}!", n - 1, sum);

                ++numNonAddToNMinusOne;
            }
        }
    }

    /**
     * Summarize the visitor's experiences and data, and return 'true' only if
     * all is as expected.
     *
     * NOTE: this class is structured to be used both for interactive testing /
     * debugging and within JUnit tests. In a JUnit context the ERROR conditions
     * below would normally have been caught with assertXYZ() functions ... but
     * here we take a different approach and print out specific errors and
     * return an overall "OK" or "not OK" status -- JUnit will work only at the
     * level of this "overall" status.
     *
     * @return 'true' only if all was as expected for the given 'n' value
     */
    public boolean summarizeResultAndIndicateWhetherExpectedOutcome() {

        logger.info("summarizing visitor experience for n=={}", n);

        logger.info("  numOverallVisits           : {}", numOverallVisits);
        logger.info("  numNullVisits              : {}", numNullVisits);
        logger.info("  numNonNullVisits           : {}", numNonNullVisits);
        logger.info("  numOneNodeCircularLists    : {}", numOneNodeCircularLists);
        logger.info("  numTwoNodeCircularLists    : {}", numTwoNodeCircularLists);
        logger.info("  numNonOneOrTwoNodeLists    : {}", numNonOneOrTwoNodeLists);
        logger.info("  numNonAddToN               : {}", numNonAddToNMinusOne);

        boolean allOk = true;

        if (0 == n && (1 != numNullVisits || 1 != numOverallVisits)) {
            allOk = false;
            logger.error("  ERROR:  for n=={} expected 1 null-visits and 1 overall-visits but got {} and {} instead", n,
                    numNullVisits, numOverallVisits);
        }

        if (0 < n) {

            if (0 != numNullVisits) {
                allOk = false;
                logger.error("  ERROR:  for n=={} expected 0 null-visits but got {} instead", n, numNullVisits);
            }

            final int expectedOverallVisits = (n + 1) / 2;
            if (expectedOverallVisits != numOverallVisits) {
                allOk = false;
                logger.error("  ERROR:  for n=={} expected {} overall-visits but got {} instead", n,
                        expectedOverallVisits, numOverallVisits);
            }

            final int expectedOneNodeCircularLists = 0 == n % 2 ? 0 : 1;
            if (expectedOneNodeCircularLists != numOneNodeCircularLists) {
                allOk = false;
                logger.error("  ERROR:  for n=={} expected {} one-node-circular-lists but got {} instead", n,
                        expectedOneNodeCircularLists, numOneNodeCircularLists);
            }

            final int expectedTwoNodeCircularLists = n / 2;
            if (expectedTwoNodeCircularLists != numTwoNodeCircularLists) {
                allOk = false;
                logger.error("  ERROR:  for n=={} expected {} two-node-circular-lists but got {} instead", n,
                        expectedTwoNodeCircularLists, numTwoNodeCircularLists);
            }
        }

        if (0 != numNonOneOrTwoNodeLists) {
            allOk = false;
            logger.error(
                    "  ERROR:  always expect only to visit null, one-, or two-node circular lists, found {} non-null nonconforming lists instead",
                    numNonOneOrTwoNodeLists);
        }

        if (0 != numNonAddToNMinusOne) {
            allOk = false;
            logger.error("  ERROR:  expected all circular-list visits to result in sum {}, got {} non-matching results",
                    n, numNonAddToNMinusOne);
        }

        if (allOk) {
            logger.info("  no unexpected conditions found");
        }
        else {
            logger.error("  ERROR:  at least one unexpected condition found");
        }

        return allOk;
    }
}
