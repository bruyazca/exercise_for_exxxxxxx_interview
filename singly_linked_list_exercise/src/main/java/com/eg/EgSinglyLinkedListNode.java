package com.eg;

import com.eg.singlylinked.SinglyLinkedListNode;

/**
 * An example subclass for a singly-linked list that holds a plain integer per
 * node.
 *
 * Note that 'SinglyLinkedListNode' could have been genericized as well to hold
 * reference to some kind of 'data' ... but that would make the implementation
 * require yet one more dereference. There are other tradeoffs as well ... not
 * going into most details here, but not that that approach would have made
 * no-longer-necessary the need to explicitly subclass the node as we're doing
 * here.
 */
public class EgSinglyLinkedListNode extends SinglyLinkedListNode {

	private final int value;

	/**
	 * Constructs a node element with a constant value ... we assume this value
	 * never changes, for now
	 *
	 * @param value
	 *            the value of the node
	 */
	public EgSinglyLinkedListNode(final int value) {
		super();
		this.value = value;
	}

	@Override
	public EgSinglyLinkedListNode getNext() {

		// less chance for client code to make a mistake if we use the cast here
		// in the subclass

		return (EgSinglyLinkedListNode) super.getNext();
	}

	public int getValue() {
		return value;
	}
}
