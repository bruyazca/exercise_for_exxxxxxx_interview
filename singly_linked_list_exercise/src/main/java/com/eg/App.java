package com.eg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eg.singlylinked.SinglyLinkedListNodeSpecialOps;

/**
 * Hello world!
 */
public class App {

    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static final int MAX_NUM_NODES_FOR_EXAMPLE = 10000;

    /**
     * Make an example singly-linked list using our node, with nodes ranging in
     * value starting with '0' to 'n-1' as traversing through the list. The
     * resulting list is not circular.
     *
     * @param n
     *            the number of nodes in the example singly-linked non-circular
     *            list
     * @return the list
     */
    public static EgSinglyLinkedListNode makeExampleSinglyLinkedListWithNNodes(final int n) {

        EgSinglyLinkedListNode curHead = null;

        for (int ii = n - 1; ii >= 0; --ii) {

            final EgSinglyLinkedListNode newHead = new EgSinglyLinkedListNode(ii);
            newHead.setNext(curHead);
            curHead = newHead;
        }

        return curHead;
    }

    /**
     * An item giving some basic info about the list as printed and analyzed in
     * dumpExampleSinglyLinkedListPerhapsMadeCircular() below. We avoid standard
     * getter/setter.
     */
    public static class ListAnalysis {

        /**
         * number of nodes in the list
         */
        public int numNodes = 0;

        /**
         * whether the list is circular ... 'null' indicates irrelevant (list is
         * null)
         */
        public Boolean isCircular = null;
    }

    /**
     * Dump the singly-linked list, analyze number of nodes and whether or not
     * the list has been made into a circular list.
     *
     * @param msg
     *            a message explaining context for the dump
     * @param list
     *            the singly-linked list to dump (NODE: may or may not have been
     *            formed into a circular list)
     * @return a list analysis, indicating number of nodes, and whether the list
     *         is or is not circular
     */
    public static ListAnalysis dumpExampleSinglyLinkedListPerhapsMadeCircular(final String msg,
            final EgSinglyLinkedListNode list) {

        logger.debug("== BEGIN dump singly-linked (perhaps made 'circular'!) list in context [{}] ==", msg);

        final ListAnalysis analysis = new ListAnalysis();

        if (null == list) {
            logger.debug("  == END **NULL** ==");
            return analysis;
        }

        final EgSinglyLinkedListNode firstNode = list;
        EgSinglyLinkedListNode curNode = list; // needs to last beyond loop
        for (; null != curNode && (firstNode != curNode || 0 == analysis.numNodes); curNode = curNode.getNext()) {

            ++analysis.numNodes;

            logger.debug("  {}", curNode.getValue());
        }
        analysis.isCircular = (null != curNode);

        logger.debug("  == END, num_nodes [{}] circular? [{}] ==", analysis.numNodes, analysis.isCircular);

        return analysis;
    }

    public static void main(final String[] args) {

        // -- get number of nodes for the particular case the user's asking for

        // NOTE: we're choosing a log-error-and-return rather than a
        // throw-exception failure mode for bad 'N' input -- depending on
        // requirements we could have gone a different way

        if (args.length < 1) {
            logger.error("Must supply test case value for 'N'");
            return;
        }

        int n = -1;
        try {
            n = Integer.parseInt(args[0]);
        }
        catch (final Throwable ignoreParticularsHere) {
            // will be handled below
        }
        if (n < 0) {
            logger.error("Value [{}] supplied for 'N' isn't a valid integer or else is < 0", args[0]);
            return;
        }
        if (n > MAX_NUM_NODES_FOR_EXAMPLE) {

            // put in this check for good measure -- we choose to have a limit

            logger.error("Value [{}] supplied for 'N' is larger than the maximum allowable [{}]", n,
                    MAX_NUM_NODES_FOR_EXAMPLE);
            return;
        }

        // -- let's run the case twice, once where we do the operation without a
        // visitor (technically "using no additional memory") and once with a
        // visitor that in our case does not accumulate a list of resulting
        // circular lists but conceivably could

        for (int ii = 0; ii < 2; ++ii) {

            final boolean runWithoutVisitor = 0 == ii;

            // -- construct the singly-linked list

            logger.info("Constructing a singly-linked list with 'N' nodes");
            final EgSinglyLinkedListNode singlyLinked = makeExampleSinglyLinkedListWithNNodes(n);

            if (runWithoutVisitor) {

                // dump list only first only once ... presumably list will be
                // equivalent in second run

                dumpExampleSinglyLinkedListPerhapsMadeCircular("initial list", singlyLinked);
            }

            if (runWithoutVisitor) {

                try {
                    SinglyLinkedListNodeSpecialOps.specialSinglyLinkedListOperation(singlyLinked);
                }
                catch (final Throwable t) {
                    logger.error("Unexpected exception while running algorithm on list with n==" + n, t);
                }
            }
            else {

                final EgVisitor visitor = new EgVisitor(n);
                try {
                    SinglyLinkedListNodeSpecialOps.specialSinglyLinkedListOperation(singlyLinked, visitor);
                }
                catch (final Throwable t) {
                    logger.error("Unexpected exception while running algorithm on list with n==" + n, t);
                }

                visitor.summarizeResultAndIndicateWhetherExpectedOutcome();
            }
        }
    }
}
