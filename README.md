# ... An exercise for E\*\*\*\*\*\*\* interview, requested by A\*\*\*\* G\*\*\*\*\*\*\*\* ...

The exercise involves a special procedure carried out on singly-linked lists.  More details about the special procedure will be forthcoming ...  The solution is implemented in Java, using JUnit for automated test cases.

## The Singly-Linked List Implementation

The 'intrusive' singly-linked list node type is defined at [SinglyLinkedListNode.java](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/singly_linked_list_exercise/src/main/java/com/eg/singlylinked/SinglyLinkedListNode.java), to be subclassed for particular purposes.

For algorithm development, debugging, and automated testing purposes, the class **SinglyLinkedListNode.java** was subclassed by [EgSinglyLinkedListNode.java](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/singly_linked_list_exercise/src/main/java/com/eg/EgSinglyLinkedListNode.java).  This version of the singly-linked list node adds an integer value used to identify the node in test output.

## Main Program File - App.java

The main application class [App.java](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/singly_linked_list_exercise/src/main/java/com/eg/App.java) contains:

* a **main(...)** function that lets one exercise the special singly-linked list operation on a singly-linked list of length 'n', where 'n' is passed in on the command line
* a function **makeExampleSinglyLinkedListWithNNodes(n)** that will create an example singly-linked list of length 'n'
* a function **dumpExampleSinglyLinkedListPerhapsMadeCircular(headNode)** that will print the contents of a singly-linked list (note ... the circular-list detection features there aren't really used anywhere)

## JUnit Test Cases and Handled Test Conditions

Note that the **App.java** **main()** program was useful for developing and debugging the code, but **the JUnit test cases appearing** at [AppTest.java](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/singly_linked_list_exercise/src/test/java/com/eg/AppTest.java) cover a more comprehensive set of test cases exercising the algorithm, including:

* a test for a singly-linked list with size n = 0 (i.e., 'null')
* singly-linked list sizes n in range 1..10
* a test to ensure that unexpected circular lists are handled appropriately by the algorithm (Exception thrown)
* a test to ensure that not-necessarily-circular-lists-that-nevertheless-contain-cycles are handled appropriately by the algorithm (Exception thrown)
* (NOTE:  due to the way the algorithm was developed, this circular / cycle check came almost "for free" without much added code or complexity.)

## The Actual Algorithm

The special algorithm is defined in file [SinglyLinkedListNodeSpecialOps.java](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/singly_linked_list_exercise/src/main/java/com/eg/singlylinked/SinglyLinkedListNodeSpecialOps.java).  This file contains two entry points to the algorithm:

* function **specialSinglyLinkedListOperation(singlyLinkedNonCircularList, visitor)** is the main implementation of the special operation to be implemented in this exercise ... it allows a special "visitor" parameter that can analyze each generated circular list for correctness (*this function fully documents the intent and results of the operation*)

* another form of the function **specialSinglyLinkedListOperation(singlyLinkedNonCircularList)** that eliminates the 'visitor' (actually calls the other form with visitor == null)

* (NOTE:  one might ask why the function uses a 'visitor' -- the goal of the exercise is to destructively split a singly-linked list into multiple circular lists of at most two nodes apiece, without creating additional transient data structures (presumably other than simple local variables within the function itself).  To make sure the algorithm complies, it does not accumulate generated circular lists into a result list (this would involve additional memory / side-data-structures for the return value, unless a second set of "pointers" were added to the nodes).  In any case, the 'visitor' allows QA code to analyze the generated circular lists, to accumulate information across them all, and to verify correctness.)

The Java interface any visitor must conform to is defined in [SpecialSinglyLinkedListOperationResultListVisitor.java](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/singly_linked_list_exercise/src/main/java/com/eg/singlylinked/SpecialSinglyLinkedListOperationResultListVisitor.java).  Note the special argument to function **visitOneResultList(generatedListHeadNode)** with value 'null' if the original list input to **specialSinglyLinkedListOperation(singlyLinkedNonCircularList)** is 'null'.  Otherwise, a particular visitor will get a non-null argument in this function for all generated circular lists.

## The "Test Case Visitor"

The JUnit test cases employ the visitor defined at [EgVisitor.java](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/singly_linked_list_exercise/src/main/java/com/eg/EgVisitor.java).  The JUnit test cases pass this visitor into the special algorithm so that the visitor can analyze each circular list.  The JUnit test cases also ask the visitor to do some post-algorithm summaries and verification.  The verifications include:

* ensuring proper number and nature of calls when the input to the special algorithm is n=0, i.e., 'null'
* ensuring that every circular list is actually a one- or two-node circular list
* ensuring that in every two-node circular list that the proper nodes in the original singly-linked list are paired together
* various other checks

See:

* function **visitOneResultList(oneOrTwoNodeCircularListHead)** to see data accumulated and checks perform for each one- or two-node circular list
* function **summarizeResultAndIndicateWhetherExpectedOutcome()** for post-algorithm-run verification and summarization

## Running the Test Cases

To run the test cases, clone this Bitbucket 'git' repository to your local system, make sure "maven" is installed, go to the './singly_linked_list_exercise' directory, and type:  **mvn test**.  You should see output for n=0, n in range 1..10, and also special inputs with circular or with-cycle lists.  *The output does contain two exceptions, but **these are expected**!*

## Test Case Output

An annotated version of the test case output is stored at [TEST_OUTPUT.txt](https://bitbucket.org/bruyazca/exercise_for_exxxxxxx_interview/src/master/TEST_OUTPUT.txt).

To make sure the output is clear, we annotate one typical case here, n = 7:

    INFO  AppTest - ======== Running test with n = 7 ========
    INFO  AppTest - 

        >>>> the test case generates and dumps the input list (in this
             case with 7 nodes)

    INFO  AppTest - generating list with nodes [0..6]
    INFO  AppTest - 
    DEBUG App - == BEGIN dump singly-linked (perhaps made 'circular'!) list in context [test list for n = 7] ==
    DEBUG App -   0   
    DEBUG App -   1   
    DEBUG App -   2   
    DEBUG App -   3   
    DEBUG App -   4   
    DEBUG App -   5   
    DEBUG App -   6   
    DEBUG App -   == END, num_nodes [7] circular? [false] ==

        >>>> the test calls the special algorithm with the test-case "visitor"

    INFO  AppTest - 
    INFO  AppTest - *** Running special algorithm with a visitor to analyze all circular lists and summarize results *** 

        >>>> the special algorithm calls the test-case "visitor" for
             each and every circular list created, and checks for various
             conditions, including:  one- or two-node, full circularity,
             proper combination in two-node cases (actually also in one-
             node cases, collapsing the checks for both into one test), etc.

    DEBUG EgVisitor - visit 0 for case n==7
    DEBUG EgVisitor -   visit:  one-node circular list value==3      << one-node

    DEBUG EgVisitor - visit 1 for case n==7
    DEBUG EgVisitor -   visit:  two-node circular list values==0,6   << two-node

    DEBUG EgVisitor - visit 2 for case n==7
    DEBUG EgVisitor -   visit:  two-node circular list values==1,5   << two-node

    DEBUG EgVisitor - visit 3 for case n==7
    DEBUG EgVisitor -   visit:  two-node circular list values==2,4   << two-node

        >>>> the test case asks the visitor, finally, after the special algorithm
             is completely done, to summarize its experiences and to make
             sure that all the counters below have their expected values --
             any failure would result in a failed JUnit test case

    INFO  EgVisitor - summarizing visitor experience for n==7
    INFO  EgVisitor -   numOverallVisits           : 4 
    INFO  EgVisitor -   numNullVisits              : 0 
    INFO  EgVisitor -   numNonNullVisits           : 4 
    INFO  EgVisitor -   numOneNodeCircularLists    : 1 
    INFO  EgVisitor -   numTwoNodeCircularLists    : 3 
    INFO  EgVisitor -   numNonOneOrTwoNodeLists    : 0 
    INFO  EgVisitor -   numNonAddToN               : 0 
    INFO  EgVisitor -   no unexpected conditions found  <<< indicates no "domain-errors" found
    INFO  AppTest - 
    INFO  AppTest - 

## Conclusion

The exercise solution represented in this 'git' repository in Bitbucket:

* represents a solution to the outlined algorithm problem as presented in the interview
* is implemented in Java using standard Java build / test tools, and using Eclipse as an IDE, and follows some standard Java best practices
* includes a main(...) program allowing quick development of the solution ... as a preliminary step to developing actual good JUnit test cases
* contains a full set of JUnit test cases exercising what should be a good set of test cases, *including corner cases (0-size input) and unexpected cases (circular or with-cycle input)*
* those JUnit test cases include some very comprehensive checks covering multiple angles of what rightly should be verified during the algorithm development

I hope the solution provides some insight into the coding practices of this developer and thought into handling unexpected corner cases, and thought presented in development and testing.

Feel free to ask the author questions about the algorithms, practices here, or approach.

# === END ===